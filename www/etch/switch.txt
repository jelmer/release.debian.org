DRAFT!!

To: team@security.debian.org, secure-testing-team@lists.alioth.debian.org
Cc: xxxxxx@release.debian.net, xxxxxx@release.debian.net
Subject: Phase of NO-DSAs, 3.1r6 to 4.0r0 switch

Hi security team members,

our current draft for the release of Etch this weekend is to get finalized, but
we have some minor issues which need to be clarified before:

+ 3.1r6 will be released as OLDstable on Satarday evening (with the 7:52pm mirror
  pulse), which means, being prepared afther the 7:52am mirror pulse on Saturday.

+ 4.0r0 will be (if everything goes well) released with Sunday's 7:52pm mirror
  pulse as stable.

This means for security: there should please be NO DSAs between Saturday 7am
and the release of Etch. This means probably also there should be no DSAs
pending in your queue for Sarge by Saturday morning 7am, otherwise they would
be released for the wrong suite.

Could you please also give me some feedback how much DSAs are still pending
currently?

Greetings
Martin
