To: debian-devel-announce@lists.debian.org
Subject: Release update: kde3.3, upload targets, infrastructure

Hello, world.

After almost three weeks since the last update, the status of the
release is as follows.  We would have liked to present sarge as a
Christmas present, but regrettably that didn't work out.  We still hope
that those of you who celebrate Christmas have a Merry Christmas, and we
wish you all a Happy New Year.


Blocker number 1 is still that testing-security is not available. Please
see the last release update for details of what needs to be done to make
it available.  Since then, a raw patch for the most urgent changes in
katie (the archive maintenance software) has been produced.  This patch
is currently being refined, and some more testing needs to take place.
Once this change is done, the most urgent blocker for bringing up
testing-security will be solved.  Testing-proposed-updates will be fully
usable at about the same time as testing-security.


The most recent kernel security fixes inadvertently introduced a kernel
ABI change, with implications for the sarge installer.  The kernel and
installer teams are still in the process of working through this issue;
we expect there will be a minor update to d-i RC2 before sarge's release
to incorporate these kernel security fixes once they are finalized.
This installer update will also include other small fixes for bugs found
in RC2, but should not require a complete d-i development cycle as
previous installer releases did.


The transition of GNOME 2.8 into sarge has been finished.  It was the
smoothest transition ever seen for a change of its magnitude.


The KDE maintainers have presented a plan for getting KDE 3.3 into
testing that meets the release team's requirements for avoiding
temporary breakage of testing in the process.  As a result, a final
upload of kdelibs 3.3.1 is expected soon, after which the path should be
clear for all of KDE 3.3 to enter testing within a week or two.


Another minor change of the toolchain for ia64 is pending.  Gcc-3.4
started to use libunwind, but its internal version was buggy, so the
libunwind shared library had to be built from the libunwind sources.
Since we don't regard introducing new base packages for sarge as an
option (due to debootstrap considerations), the shared libunwind library
had to be included in libgcc1 and libgcc's shlibs dependency had to be
tightened to reflect this change.  The unwind patches have been
backported to gcc-3.3, and a new glibc will follow, once the new gcc
upload is finished.  The bad news is that the current situation blocks
testing promotion for quite a few packages; we recognise this as a
problem, and fixing it is a high priority.


There has been some work regarding potential upgrade kernels (or
backports of packages) on some architectures, but these investigations
are not finished yet.


Package uploads for sarge should continue to be made according to the
following guidelines:

  - If your package is frozen, but the version in unstable contains no
    changes unsuitable for testing, upload to unstable only and contact
    debian-release@lists.debian.org if changes need to be pushed through
    to testing.

  - If your package is frozen and the version in unstable includes
    changes that should NOT be part of sarge, contact
    debian-release@lists.debian.org with details about the changes you
    plan to upload (diff -u preferred) and, with approval, upload to
    testing-proposed-updates.  Changes should be limited to translation
    updates and fixes for important or RC bugs.

  - If you need to do a bug fix directly in sarge, you will need to
    upload to testing-proposed-updates as well, with the same
    requirements as for frozen packages.

  - All other package updates should be uploaded to unstable only.
    Please use urgency=high for uploads that fix RC bugs for sarge
    unless you believe there is a significant chance of breakage in the
    new package.

If you want to increase the urgency of an already uploaded package,
please speak with the release team; this can be sorted out without the
need for another upload.



Cheers,
-- 
NN
Debian Release Team
