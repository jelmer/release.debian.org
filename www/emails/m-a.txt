To: debian-project
Subject: Release Team meets in Germany

Hi

The Debian Release Team will organise a meeting in Germany right before
Debcamp. Andreas Barth, Adeodato Simo, Marc Brockschmidt, Luk Claes and
Martin Zobel Helas will brainstorm about how the Lenny release cycle
can work even better than the Etch release cycle worked. This of course 
includes identifying release goals and blockers; and getting a first 
idea of the release schedule. You will be able to read more about the
actual content of the meeting in the release update that will be posted
after the meeting :-)

Cheers

Luk Claes
Debian Release Team
