To: debian-devel-announce@lists.debian.org
Subject: Etch in December 2006

Hi all,

thank you all for your help and your warm welcome of the release of sarge.
This release was only possible as common effort of the developers, the
infrastructure teams and a lot of more involved people.  Thank you all for
making this possible.


Etch in December 2006
~~~~~~~~~~~~~~~~~~~~~
Now that Debian has released, our goal is to make sure that the next
release happens on time and that we manage to resolve for etch as many of
the issues that delayed sarge as possible.  To make it easier for all of us,
we decided to make a long-term timeplaning.

We will release etch in December 2006.  The base-freeze has worked out well,
but we want to include fewer packages in it and to have it shorter. So, we
start with the freeze end of July 2006 with freezing the essential
toolchain and the kernels.  One week later we freeze base and the
non-essential toolchain.  Directly after that, the (hopefully final)
installer release candidate is published.  We plan to have the general
freeze about the same time span as now, that means, it starts in October
2006.  Of course, during that time we need also to keep the number of
release critical bugs well below a certain treshhold. Of course, squashing
release critical bugs needs to start as soon as possible, that is now.


Release blockers
~~~~~~~~~~~~~~~~
Of course, we can do a mostly time-based release only if we don't put more
release blockers on our tablet than we are able to finish off. For that
reason, we restricted the possible blockers to:
- toolchain transition
- xorg
- amd64 as an official arch (and the mirror split as pre-condition for that)
- sorting out docs-in-main vs. the DFSG
- sorting out non-free firmware
- secure apt

The toolchain transition already started. Currently, some packages in
unstable FTBFS because they need the newer glibc from testing.  That is
unfortunate, but happens sometimes during a transition.  Of course, we must
make sure that these issues are resolved as soon as possible, and this
means here that the newer glibc goes into unstable soon.

Xorg is already on its way to etch, so this probably won't hold up us from
the release. Secure apt has hit unstable also.  For sorting out the
freeness issues, Frank Lichtenheld will coordinate a bit; please see his
messages to the debian-qa list for details.

So, we are pretty optimistic that none of the release blockers will
actually block our release.


Pet release goals
~~~~~~~~~~~~~~~~~
We know that there is more that most of us want to see in etch. We started
to call these issues "pet release goals". That doesn't mean that you can
safely ignore them, and we reserve the right to make any of them a release
blocker (aka ignoring them makes packages RC-buggy) if there is not much
left to finish them.  Also, it's quite possible that some of them become
release blockers for etch+1.  The list of pet release goals is:
- building everything with libselinux
- finishing /usr/doc
- pervasive LFS (large files) support
- symbol versioning in libraries where it's needed
- rethinking how we handle lib dependencies (see discussion on
  debian-devel)
- dependency-based init
- a true system locale that doesn't use the /etc/environment configfile.
  We can then localize the entire boot process.
- utf-8 default
- manifest-like binary: header in .dsc that's actually correct
- fixing udeb sync issues and testing migration
- kernel: not more than 2 versions; 1 source package for all archs
- getting rid of circular deps (in the general case; there are corner
  cases in essential)


Updates to the release policy
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Based on our experiences with sarge, we changed the release policy:

1. All content in main and contrib must meet the DFSG

   Of course, this had to happen, based on the recent GRs.

2. [The usual] targets must not change the package's build-dependencies or
   the changelog.
   
   We found out that some packages do, which is of course bad style,
   because it makes harder to re-produce the build that happened on some
   buildd.

3. MTAs which do not provide the -bs switch must Conflict: with the lsb
   package.

   That was what we accepted for sarge anywys, but now we wrote it
   explicitly into the release policy.

We want to move to a more recent LSB compatibility level, but that needs
some more discussions with the Debian LSB team.  Also, the current python
policy needs reshaping, but that is something where the content should come
from the python team, and not from the release team.

The etch release policy is available from
http://release.debian.org/etch_rc_policy.txt.


Issues for the release notes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
If you have anything which should be in the release notes for etch, please
file a bug right now.  The release notes are now available as
pseudo-packages.


More lessons from sarge
~~~~~~~~~~~~~~~~~~~~~~~
There is something more we should learn together from sarge:

[TBD]


So, thank you all for your help with the release of sarge. And let's make
it even better with etch.



Cheers,
Andi
