Subject: testing d-i Release Candidate 1 and more release adjustments

Congratulations to the Debian Installer team for publishing their first
Release Candidate for Etch![1]  This means it's time for two things:  to
ask users to begin more extensive testing of Etch as a system, and to
revisit our release schedule to see where we are.


Release timeline
================
The proposed release schedule included an expected date for RC1 in the
middle of August, which means that - by this measure - we're three months
behind.  The release schedule did include some padding and we can reduce
the time between RC1 and the release, but not by a whole three months -
especially since we (and the kernel team) want to include Linux 2.6.18 for
Etch, which will require a second installer release candidate before
release.

We have also reviewed our status with respect to the timeline in other
areas.  The good news is that overall, Etch is in good shape.  New bug
reports about release-critical issues are still coming in as expected, but
with recent rounds of bug filing for build failures happening even before
the start of the full freeze, and our better tracking of bugs fixed only in
unstable as a result of version-tracking in the BTS, we are confident that
the RC bug graph[2] genuinely shows that we are converging on a release.
We know for sure that release-critical bugs are being fixed faster than
they're being introduced, which is great news for our users.  Good job,
developers!  Please keep up the great work of helping the release and our
users by fixing release critical bugs and, heck, any other bugs too.

Nevertheless, it would not be possible to release etch on the original
target date while also meeting the Debian community's expectations of
quality.  We do expect to freeze the full archive soon now that the
installer RC 1 is out, which means a freeze delay of about one month from
the original projection, and a similar delay for the release.


Call for testing
================
With the installer candidate out and an initial draft of the release notes
available[3], we can use some help now from intrepid users doing upgrade
testing from sarge to etch.  While security support for embargoed issues is
not yet available for etch and there are release-critical bugs yet to
resolve, we're happy to remind you that even before the freeze, testing is
of very high quality; there will still be changes for bugs, but even before
it's released as stable we are very proud to have you test it with the
confidence that it will be a very usable and high-quality system -- just
not quite the perfection we look for in a release. ;)

It is our goal to have full security support for testing available shortly
after the start of the hard freeze.  Until then, we can't recommend
upgrading systems to etch on the public Internet, but we will welcome any
testing help you can provide.  Please help us by reporting problems you
find in the upgrade process against the upgrade-reports package, issues you
believe should be documented in the release notes against the release-notes
package, and bugs found in individual packages after upgrade to those
packages.


The soft freeze continues
=========================
We have not yet reached a point where we believe there should be a hard
freeze of updates into etch, but we ask your continued support by avoiding
uploads that are likely to introduce regressions or be disruptive to other
packages so close before release.  Please do not upload new upstream
releases or start library transitions without discussing with the release
team, and please don't upload changes to unstable that are not meant for
Etch.  You can always upload such changes to experimental.

The release team depends on your cooperation with these guidelines in order
to keep the Etch release moving forward at the present pace.  If too many
new release-critical bugs are added because maintainers ignore these
guidelines, we will have to consider enacting a hard freeze in spite of the
bug count -- which means more administrative work for the release team, a
longer freeze and a later release.  So if you want to see an etch release
this year, please consider the impact that your uploads will have on this
timeline.


Updates to frozen packages
==========================
We have previously noted the kinds of fixes that are allowed into Etch for
packages that are frozen[4].  Please remember that while the release team
will track the status of packages fixing release critical bugs, if you want
a frozen package to be updated because of severity: important bugfixes or
documentation/l10n changes, it is your responsibility to notify the release
team by sending mail to debian-release@lists.debian.org.


Release goals and etch-ignores
==============================
After a mailing list discussion about release-criticality and policy, we've
decided to include "fixing bashisms" as a release goal.  This means that if
packages have bugs that render them unusable with other implementations of
/bin/sh, such as dash, we encourage maintainers to consider NMUing those
packages under the same rules that govern release-critical bugfixes.[5]
This does not mean that such bugfixes *are* release-critical (they're
severity: important unless there is another reason they should be release
critical), so such fixes will be included in Etch on a best-effort basis.

Remember that other non-RC policy violations, while not covered by the
release team's 0-day NMU policy, might still be candidates for NMUs; please
see the Developer's Reference[6] for recommendations on NMUing.

We have also decided to ignore two certain class of bugs for etch that
would nominally be considered RC.  First, packages should not rely on
non-essential packages during postrm/purge, but bugs about packages that
fail to purge when debconf is not installed will be tagged etch-ignore as
long as the package has a correct dependency on debconf otherwise.  Failing
to purge by calling other, less prevalent non-essential tools will still be
RC; please check the postrms of your packages, don't wait for someone to
file a bug!

Second, newly-detected RFCs in source packages will also be treated as
etch-ignore, though they will certainly be release-critical for etch+1.

Please still feel free to fix any etch-ignore bugs for the release.  As
with release goals, we will accept these fixes on a best-effort basis.  You
will need to prod the release team to allow the package into Etch for such
fixes if the package is frozen, but we are happy to allow these fixes in.


You can't have pudding if you don't eat your meat
=================================================
If you managed to read so far: Congratulations!  That means you get to be
one of the lucky first 10 people to know the codename for the next Debian
release:  it will be called Lenny.


Summary
=======
Please test the debian-installer Release Candidate 1.

Please still follow [4]: No new upstream releases and no transitions,
unless explicitly agreed by the release team. Also, please don't upload
changes that are not meant for Etch to unstable. You can of course use
experimental for any changes, however experimental they are.  Please keep
that policy for unstable until after the release of Etch, even during hard
freeze.

The release will probably be delayed by about the same as the full freeze,
that is a month. We hope to still release in December 2006.


Cheers,
NN
Debian Release Team


[1] http://lists.debian.org/debian-devel-announce/2006/11/msg00003.html
[2] http://bugs.debian.org/release-critical/
[3] http://www.debian.org/releases/etch/releasenotes
[4] http://lists.debian.org/debian-devel-announce/2006/11/msg00000.html
[5] http://lists.debian.org/debian-devel-announce/2006/07/msg00005.html
[6] http://www.debian.org/doc/developers-reference/ch-pkgs.en.html#s-nmu
